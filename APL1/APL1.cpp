// APL1.cpp : Defines the entry point for the console application.
//

#include<stdlib.h>
#include<stdio.h>
#include<math.h>

int isTriangle(int, int, int);
int task1();
int task2();

int main()
{
	return task2();
}


int isTriangle(int a, int b, int c)
{
	return ((a + b > c) && (a + c > b) && (b + c > a));
}

int task1()
{
	int a, b, c;

	printf("a = ");
	scanf("%d", &a);
	printf("b = ");
	scanf("%d", &b);
	printf("c = ");
	scanf("%d", &c);

	if (!isTriangle(a, b, c)) {
		printf("Triangle cannot exist.\n");
		system("pause");
		return 1;
	}

	float expr = (float)(b*b + c*c - a*a) / (float)(2 * b*c);


	float alpha = (acos(expr));
	float tanAlpha = tan(alpha);
	float p = (a + b + c) / 2;
	float bisectBeta = 2 * (sqrt((float)(a*c*p*(p - b))) / (a + c));

	printf("\nTan(alpha): %5.3f\n", tanAlpha);
	printf("Bisect from beta: %5.3f\n", bisectBeta);


	system("pause");
	return 0;


}

int task2()
{
	int a, b, c;

	printf("a = ");
	scanf("%d", &a);
	printf("b = ");
	scanf("%d", &b);
	printf("c = ");
	scanf("%d", &c);

	int count = 0;
	if ((a == b) && (a == c)) {
		count = 3;
	}
	else if ((a == b) || (a == c) || (b == c)) {
		count = 2;
	}

	printf("count of same numbers: %d\n", count);

	system("pause");
	return 0;
}

